package com.example.registro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Registro extends AppCompatActivity {

    EditText usuario,contrasena;
    Button registro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        Context context=this;
        SharedPreferences sharprefs=getSharedPreferences("ArchivoSP",context.MODE_PRIVATE);

        usuario=(EditText)findViewById(R.id.txtusuario);
        contrasena=(EditText)findViewById(R.id.txtcontrasena);
        registro=(Button)findViewById(R.id.btnregistro);

        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Usuario = usuario.getText().toString();
                String Contraseña = contrasena.getText().toString();
                Intent intent = new Intent(v.getContext(),MainActivity.class);
                intent.putExtra("miUsuario",Usuario);
                intent.putExtra("miContrasena",Contraseña);
                startActivity(intent);
            }
        });
    }
}