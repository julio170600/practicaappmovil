package com.example.registro;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class Datos extends AppCompatActivity {

    private EditText editTextNumero;
    private EditText editTextNumero2;
    private ImageButton btnNumero;
    private ImageButton btnNumero2;

    private final int PHONE_CALL_CODE = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos);

        editTextNumero = (EditText) findViewById(R.id.editTextNumero);
        btnNumero = (ImageButton) findViewById(R.id.btnNumero);
        editTextNumero2 = (EditText) findViewById(R.id.editTextNumero2);
        btnNumero2 = (ImageButton) findViewById(R.id.btnNumero2);

        btnNumero.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                String num = editTextNumero.getText().toString();
                if (num != null)
                {
                    //Comprobar la version actual
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                    }
                    else
                    {
                        versionAnterior(num);
                    }
                }
            }

            private void versionAnterior (String num)
            {
                Intent intentLlamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+num));
                if(verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(intentLlamada);
                }
                else
                {
                    Toast.makeText(Datos.this, "Configura los permisos", Toast.LENGTH_SHORT);
                }
            }
        });

        btnNumero2.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                String num2 = editTextNumero2.getText().toString();
                if (num2 != null)
                {
                    //Comprobar la version actual
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                    }
                    else
                    {
                        versionAnterior(num2);
                    }
                }
            }

            private void versionAnterior (String num2)
            {
                Intent intentLlamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+num2));
                if(verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(intentLlamada);
                }
                else
                {
                    Toast.makeText(Datos.this, "Configura los permisos", Toast.LENGTH_SHORT);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                String permission2 = permissions[0];
                int result2 = grantResults[0];

                if (permission.equals(Manifest.permission.CALL_PHONE)){
                    //Comprobar si se acepto o denego el permiso
                    if (result == PackageManager.PERMISSION_GRANTED){
                        String phoneNumber = editTextNumero.getText().toString();
                        Intent llamda = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+phoneNumber));
                        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;
                        startActivity(llamda);
                    }else{
                        //No se concede el permiso
                        Toast.makeText(this,"No aceptaste el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
                else if (permission2.equals(Manifest.permission.CALL_PHONE)){
                    //Comprobar si se acepto o denego el permiso
                    if (result2 == PackageManager.PERMISSION_GRANTED){
                        String phoneNumber = editTextNumero2.getText().toString();
                        Intent llamda = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+phoneNumber));
                        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;
                        startActivity(llamda);
                    }else{
                        //No se concede el permiso
                        Toast.makeText(this,"No aceptaste el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private boolean verificarPermisos (String permiso)
    {
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado == PackageManager.PERMISSION_GRANTED;
    }
}