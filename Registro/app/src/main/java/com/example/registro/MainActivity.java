package com.example.registro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btn1,btn2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String Usuario = getIntent().getExtras().getString("miUsuario");
        String Contraseña = getIntent().getExtras().getString("miContrasena");

        btn1 = (Button) findViewById(R.id.btnregis);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Registro.class);
                startActivity(intent);

            }
        });

        btn2 = (Button) findViewById(R.id.btningresar);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario=((EditText) findViewById(R.id.edtUsuario)).getText().toString();
                String pass=((EditText) findViewById(R.id.edtContrasena)).getText().toString();
                if (usuario.equals(Usuario)&& pass.equals(Contraseña)){
                    startActivity(new Intent(MainActivity.this,Datos.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Usuario y/o Contrasena Incorrecto",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}